// task2.cpp: определяет точку входа для консольного приложения.
//


#include <iostream>
#include <ctime>
using namespace std;
void FillArray(int*, int);
void PrintArray(int*, int);
void FormatTimeFromSecondsToMinutes(int, int &, int &);
void ShowFormattedTime(int, int, int);
void FindSpecialValues(int*, int, int&, int&, int&);
int main()
{
	srand(time(0));
	int size = rand() % 25 + 25;
	cout << "Size = " << size << endl;
	int *mass = new int[size];
	FillArray(mass, size);
	PrintArray(mass, size);

	for (int i = 0; i < size; i++)
	{
		int minutes = 0, seconds = 0;
		int Time = mass[i];
		FormatTimeFromSecondsToMinutes(Time, minutes, seconds);
		ShowFormattedTime(i, minutes, seconds);
	}

	int min, max, medium;
	FindSpecialValues(mass, size, min, max, medium);

	return 0;
}
//---------------------------------------------------------
void FillArray(int *_mass, int _size)
{
	for (int i = 0; i < _size; i++)
		_mass[i] = rand() % 150 + 30;
}
//---------------------------------------------------------
void PrintArray(int *_mass, int _size)
{
	for (int i = 0; i < _size; i++)
		cout << _mass[i] << "  ";
	cout << endl;
}
//---------------------------------------------------------
void FormatTimeFromSecondsToMinutes(int _Time, int & _minutes, int & _seconds)
{
	_minutes = _Time / 60;
	_seconds = _Time % 60;
}
//---------------------------------------------------------
void ShowFormattedTime(int _ID, int _minutes, int _seconds)
{
	if (_seconds >= 10)
	{
		cout << "Sportsman " << _ID + 1 << ": 0" << _minutes << ":" << _seconds << endl;
	}
	else
	{
		cout << "Sportsman " << _ID + 1 << ": 0" << _minutes << ":0" << _seconds << endl;
	}
}
//---------------------------------------------------------
void FindSpecialValues(int *mass, int size, int& min, int &max, int &medium)
{
	int sum = 0, min = 181, max = 29;

	for (int i = 0; i < _size; i++)
	{
		sum += mass[i];
		if (mass[i] < min)
			min = mass[i];
		if (mass[i] > max)
			max = mass[i];
	}
	medium = sum / size;
}
//----------------------------------------------------------